//
//		cookieInfo plugin v0.1
//		By Kowcio

var cookieInfo = function(name_var) {
  
	this.init(name_var);
}

$.extend(cookieInfo.prototype, {
   // object variables
   widget_name	: 	''				,
   jqObject		:	null		 	,

   
   init: function(widget_name) {
     // do initialization here
     this.widget_name = widget_name;
     //this.widget_name = $("#jqCookie");
   },

   
   doSomething: function() {
     // an example object method
     alert('my name is '+this.widget_name);
   },
   
   
   tekstTest: function( txt ) {
	     // an example object method
	     return txt;
	},
	
	
    //some day make it inside the code below
    writeCookieWithOKInfo: function( ack ) {
    	cookieExpires = 365;
    	cookiePath = "/";
    	cookieName ="acceptCookies";
    	var cookieVar = {				//define cookei with data for the chart
    			acceptCookies : ack ,
    	} ;
    	$.cookie(cookieName, cookieVar  , {
			   expires : cookieExpires,          //expires in 365 days
	//		   path    : cookiePath,   //The value of the path attribute of the cookie 
			   domain  : cookieDomain, //The value of the domain attribute of the cookie
			   secure  : false          //If set to true the secure attribute of the cookie
			}); 	
	    // return this;
	},
	
	
    addInfo: function() {
	
	   var htmlStr = 
		   "<div id='addedCookieMsg' class='info-box' 	"+
		   "style='position:fixed;						"+
		   "float:left;									"+
		   "top:1%;										"+
		   "left:1%;									"+
		   "opacity:1;									"+
		   "width:500px;								"+
		   "height:50 px;								"+
		   "z-index:1000;'>								"+
		   "											"+
		   "<h2>We got cookies !! </h2>					"+
		   "EU thinks You should know it. Do You know it ?	"+
		   "									"+
		   "<a id='addedCookieMsgNO' class='button red' style='opacity: 1;float:right'> NO ! </a>		"+
		   "<a id='addedCookieMsgYES' class='button green' style='opacity: 1;float:right'> YES ! </a>		"+
		   "											"+
		   "</div>										";
		   	   
	   var c = $.cookie("acceptCookies" );
	   
	   if(typeof c == 'undefined' || c.acceptCookies==false){
	     $("#footer").append(htmlStr);
	   }
	   //else{   console.log("No cookies");	   }
	     
	     $("#addedCookieMsgYES").click(function(){ 
	    	 $('#addedCookieMsg').fadeOut(1500);
	     	cookieExpires = 365;
	    	cookiePath = "/";
	    	cookieName ="acceptCookies";
	    	cookieDomain ="";
	    	var cookieVar = {				//define cookei with data for the chart
	    			acceptCookies : true ,
	    	} ;
	    	$.cookie(cookieName, cookieVar  , {
				   expires : cookieExpires,          //expires in 365 days
		//		   path    : cookiePath,   //The value of the path attribute of the cookie 
				   domain  : cookieDomain, //The value of the domain attribute of the cookie
				   secure  : false          //If set to true the secure attribute of the cookie
				}); 	    	 //save info show cookies always ar redirect 
	    	 
	     });
	     $("#addedCookieMsgNO").click(function(){ 
	    	 $('#addedCookieMsg').fadeOut(1500);
	     	cookieExpires = 365;
	    	cookiePath = "/";
	    	cookieName ="acceptCookies";
	    	cookieDomain ="";
	    	var cookieVar = {				//define cookei with data for the chart
	    			acceptCookies : false ,
	    	} ;
	    	$.cookie(cookieName, cookieVar  , {
				   expires : cookieExpires,          //expires in 365 days
		//		   path    : cookiePath,   //The value of the path attribute of the cookie 
				   domain  : cookieDomain, //The value of the domain attribute of the cookie
				   secure  : false          //If set to true the secure attribute of the cookie
				}); 	    	 //save info dont show cookie more
	     });
	     
	     
    }
});

   
	
	
	
	
	
	
	

// example of using the class built above
/*


var widget1 = new cookieInfo('widget one');
widget1.doSomething();


*/






